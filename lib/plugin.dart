
import 'package:flutter/services.dart';

MethodChannel _globalChannel = MethodChannel("bms_video_player_event");//连接事件通道


class _VideoPlugin{
  Future<void> play(url) async {
    _globalChannel.invokeMethod("loadUrl",url);
  }
}