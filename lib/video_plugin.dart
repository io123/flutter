import 'dart:async';

import 'package:flutter/services.dart';
typedef void BmsVideoPlayerCreatedCallback(VideoPlayerController controller);
class VideoPlayerController {
  static const CHANNEL_NAME = "bms_video_player_event";
  static const MethodChannel methodChannel = const MethodChannel(CHANNEL_NAME);
  static const EventChannel eventChannel = const EventChannel(CHANNEL_NAME + "event_channel");
  VideoPlayerController() {
   // methodChannel =  new MethodChannel('bms_video_player_event');
   // _channel.invokeListMethod('loadUrl', "http://vfx.mtime.cn/Video/2019/03/13/mp4/190313094901111138.mp4");
  }

//  Future<void> loadUrl(String url) async {
//    assert(url != null);
//    return _channel.invokeMethod('loadUrl', url);
//  }

  static Future<void> play(String url){
    if(url==null||url.isEmpty){
      url="http://vfx.mtime.cn/Video/2019/03/13/mp4/190313094901111138.mp4";
    }
    methodChannel.invokeMethod("loadUrl",url);
  }
}
