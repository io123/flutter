package com.video.video_plugin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import com.bumptech.glide.Glide;
import java.lang.reflect.Method;
import java.util.Map;
import cn.jzvd.JzvdStd;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.platform.PlatformView;

public class VideoView  implements PlatformView, MethodChannel.MethodCallHandler {
    private final JzvdStd jzvdStd;
    private final MethodChannel methodChannel;
    private final PluginRegistry.Registrar registrar;
    private  Context context;
     private  Glide glide;
     private  String thumbImageUrl;
     VideoView(Context context, int viewId, Object args, PluginRegistry.Registrar registrar) {
         System.out.println("显示视图3");
         System.out.println(registrar);
         Map map = (Map)args;
         this.thumbImageUrl=map.get("thumbImageUrl").toString();
        this.registrar = registrar;
        this.jzvdStd = getJzvStd(registrar, args);
        this.methodChannel = new MethodChannel(registrar.messenger(), "bms_video_player_event");
        this.methodChannel.setMethodCallHandler(this);
       this.context=context;
     }
    @Override
    public View getView() {
         Glide.with(context).load(thumbImageUrl).into(jzvdStd.thumbImageView);
        return jzvdStd;
    }
    @Override
    public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
        System.out.println("请求的参数"+ methodCall.arguments.toString());
        //System.out.println("methodCall"+methodCall.method.toString());
        System.out.println(result.toString());
        String url=methodCall.arguments.toString();
        playWithJzvd(url);
    }

public  void  playWithJzvd(String url){
       System.out.println("正在播放视频");
        jzvdStd.setUp(url,"正常播放");
}

    @Override
    public void dispose() {}
    private JzvdStd getJzvStd(PluginRegistry.Registrar registrar, Object args) {
        JzvdStd view = (JzvdStd) LayoutInflater.from(registrar.activity()).inflate(R.layout.jz_video, null);
        return view;

    }

    private Object getFieldValueByName(String fieldName, Object o) {
        try {
            String firstLetter = fieldName.substring(0, 1).toUpperCase();
            String getter = "get" + firstLetter + fieldName.substring(1);
            Method method = o.getClass().getMethod(getter, new Class[] {});
            Object value = method.invoke(o, new Object[] {});
            return value;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
