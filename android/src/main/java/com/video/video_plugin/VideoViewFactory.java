package com.video.video_plugin;

import android.content.Context;

import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.common.StandardMessageCodec;
import io.flutter.plugin.platform.PlatformView;
import io.flutter.plugin.platform.PlatformViewFactory;

class VideoViewFactory extends PlatformViewFactory {
    private final PluginRegistry.Registrar registrar;
    public VideoViewFactory(PluginRegistry.Registrar registrar) {
        super(StandardMessageCodec.INSTANCE);
        System.out.println("显示视图1");
        System.out.println(registrar);
        this.registrar = registrar;
    }

    @Override
    public PlatformView create(Context context, int viewId, Object args) {
        System.out.println("显示视图2");
        System.out.println(registrar);
        System.out.println(args);
        return new VideoView(context, viewId, args, this.registrar);
    }
}
