package com.video.video_plugin;
import io.flutter.plugin.common.PluginRegistry.Registrar;

/** VideoPlugin */
public class VideoPlugin  {
  public static void registerWith(Registrar registrar) {

    //这一步是向插件注册是视图
    registrar.platformViewRegistry()
            .registerViewFactory("plugins.bms_video_player/view", new VideoViewFactory(registrar));
  }

}
