//
//  Generated file. Do not edit.
//

#import "GeneratedPluginRegistrant.h"
#import <video_plugin/VideoPlugin.h>

@implementation GeneratedPluginRegistrant

+ (void)registerWithRegistry:(NSObject<FlutterPluginRegistry>*)registry {
  [VideoPlugin registerWithRegistrar:[registry registrarForPlugin:@"VideoPlugin"]];
}

@end
