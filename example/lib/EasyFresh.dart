import 'package:flutter/material.dart';
import 'package:flutter_easyrefresh/easy_refresh.dart';
import 'package:video_plugin_example/pages/IndexPage.dart';
import 'package:video_plugin_example/pages/detail.dart';
class PlatformEasyFresh extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return new _PlatformEasyFresh();
  }

}

class _PlatformEasyFresh  extends State<PlatformEasyFresh>{
  List<String> addStr=["1","2","3","4","5","6","7","8","9","0"];
  List<String> str=["1","2","3","4","5","6","7","8","9","0"];
  DateTime now = new DateTime.now();
  @override
  Widget build(BuildContext context) {
    final double topPadding = MediaQuery.of(context).padding.top;

    return new Container(
      padding: EdgeInsets.fromLTRB(0, topPadding, 0, 0),
      child: new Center(
        child: new EasyRefresh(
          header: ClassicalHeader(
              refreshedText: "下拉刷新",
              refreshingText: "正在刷新",
              refreshReadyText: "正在刷新",
              bgColor: Colors.red,
              refreshFailedText: "稍后刷新",
              infoText: "刷新时间"+ new DateTime.now().toString()
          ),
          child:ListView.builder(itemCount: str.length,
              itemBuilder: (BuildContext context,index){
                return new Container(
                  padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                  child: new IndexPage(),
                );
              }
          ),
          onRefresh:  () async{
            await new Future.delayed(const Duration(seconds: 1), () {
              setState(() {
                str.clear();
                str.addAll(addStr);
              });
            });
          },
          onLoad: () async {
            await new Future.delayed(const Duration(seconds: 1), () {
              if (str.length < 20) {
                setState(() {
                  str.addAll(addStr);
                });
              }
            });
          },
        ),
      ),
    );
  }
void _onPressed(index){
  print(index);
    Navigator.push(
      context,
     // new MaterialPageRoute(builder: (context) => new VideoDetailPage(id: index,url: "http://vfx.mtime.cn/Video/2019/03/21/mp4/190321153853126488.mp4",)),
        new MaterialPageRoute(builder: (context) => new IndexPage()),

    );
}

}