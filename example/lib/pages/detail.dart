import 'package:flutter/material.dart';
import 'package:video_plugin/BmsVideoPlayer.dart';
class VideoDetailPage extends StatefulWidget{
  final int id;
  final String url;
  VideoDetailPage({Key key,this.id, this.url}):super(key :key);
  @override
  State<StatefulWidget> createState() {
    return _VideoDetailPage();
  }
}

class _VideoDetailPage  extends State<VideoDetailPage>{
  @override
  Widget build(BuildContext context) {
    String url=widget.url;
    int id=widget.id;
    return  new MaterialApp(
      home: new Scaffold(
        body:GestureDetector(
          onHorizontalDragUpdate: _onHorizontalDragUpdate,
          onHorizontalDragEnd: _onHorizontalDragEnd,
          onTap:_onTap,
          child:  new Container(
            color: Colors.white12,
            child: new Center(
              child: Container(
                height: 300,
                child: new BmsVideoPlayer(url),
              ),
            ),
          ),
        )
      ),
    );
  }
  void _onHorizontalDragUpdate(detail){
    print(detail);

  }
  void _onHorizontalDragEnd(detail){
    print(detail);
  }
  void _onTap(){
    print("返回");
    Navigator.pop(context);
  }
}