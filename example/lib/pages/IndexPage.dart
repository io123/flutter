import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:video_plugin/BmsVideoPlayer.dart';
class IndexPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return new _IndexPage();
  }
}

class _IndexPage extends State<IndexPage> {
  final GlobalKey globalKey = GlobalKey();
  String imageUrl = "http://b-ssl.duitang.com/uploads/item/201805/13/20180513224039_tgfwu.png";
  String nickname = "马先生（昵称）";
  String title = "红尘渡劫波,一句好听的话(标题)";
  String videoUrl = "http://vfx.mtime.cn/Video/2019/03/19/mp4/190319212559089721.mp4";
  String songName="多年以后(歌曲)";
  String thumbImageUrl="http://g.hiphotos.baidu.com/image/pic/item/d52a2834349b033b95e7b4601fce36d3d539bd19.jpg";
  int viewNum=9373;///观看人数
  int commentNum=644;///评论人数
   double  screenWidth;
   double  screenHeight;
  @override
  Widget build(BuildContext context) {
    screenWidth =MediaQuery.of(context).size.width;
    screenHeight =MediaQuery.of(context).size.height;
    print(screenWidth);
    print(screenHeight);
    return new Container(
          padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
          child: Column(
            children: <Widget>[
              //显示头像和昵称
              _HeadImageNickname(imageUrl, nickname),
              //显示视频标题
              _getTitle(title),
              //显示视频
              _getVideo(videoUrl)
            ],
          ),
        );
  }
  Widget _getTitle(String title) {
    return new Container(
        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
        child: new Align(
          alignment: new FractionalOffset(0.05, 0.0),
          child: new Text(
            title,
            style: new TextStyle(
              fontSize: 16,
              color: Colors.black,
              fontWeight: FontWeight.w100
            ),
          ),
        )
    );
  }
  Widget _HeadImageNickname(String imageUrl, String nickname) {
    return new Container(
      padding: EdgeInsets.fromLTRB(10, 0, 0, 5),
      child: new Row(
        children: <Widget>[
          new ClipRRect(
            child: Image.network(
              imageUrl,
              height: 40,
              width: 40,
              scale: 8.5,
              fit: BoxFit.cover,
            ),
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(20),
                topRight: Radius.circular(20),
                bottomLeft: Radius.circular(20),
                bottomRight: Radius.circular(20)),
          ),
          new Text(
            nickname,
            style: new TextStyle(fontSize: 19,
                color: Colors.black,
                fontWeight: FontWeight.w300,
                fontFamily: "HanaleiFill"
            ),
          )
        ],
      ),
    );
  }
  Widget _getVideo(String videoUrl) {
    return new Container(
      height: 300,
      width: screenWidth,
      color: Colors.yellow,
      child: new Stack(
        children: <Widget>[
         new Container(
           child: new BmsVideoPlayer(videoUrl,thumbImageUrl: thumbImageUrl,),
         ),
          new Positioned(
            top: 250,
            child: new Container(
              padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
              child: new Text(songName,
              style: new TextStyle(fontSize: 16,
                  color: Colors.white,
              fontWeight: FontWeight.bold),
            ),
          ),),
          new Positioned(
              top: 275,
              child: new Container(
                padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                child: new Column(
                  children: <Widget>[
                    new Row(
                      children: <Widget>[
                        new Container(
                          padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                          child: new Text("$viewNum人看过",style: new TextStyle(
                            color: Colors.white,
                            fontSize: 14
                          ),),
                        ),
                        new Container(
                          child: new Text("$commentNum人评论",
                           style: new TextStyle(
                             color: Colors.white,
                             fontSize: 14
                           ),),
                        ),
                      ],
                    )
                  ],
                ),
              ))
        ],
      ),
    );
  }
  @override
  void dispose() {
    super.dispose();
  }
}
